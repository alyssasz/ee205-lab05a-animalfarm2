///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file bird.cpp
/// @version 1.0
///
/// Exports data about all birds
///
/// @author Alyssa Zhang <alyssasz@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   Feb 17 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream> 

#include "bird.hpp"

using namespace std;

namespace animalfarm {
	
void Bird::printInfo() {
	Animal::printInfo();
	cout <<                   "   Feather Color = [" << colorName( featherColor ) << "]" << endl;
	cout << std::boolalpha << "   Is Migratory = [" << isMigratory << "]" << endl;
}

const string Bird::speak() {
	return string( "Tweet" );
}


} // namespace animalfarm