///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file palila.cpp
/// @version 1.0
///
/// Exports data about all palila birds
///
/// @author Alyssa Zhang <alyssasz@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   Feb 17 2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "palila.hpp"

using namespace std;

namespace animalfarm {
	
Palila::Palila(string whereFound, enum Color newColor, enum Gender newGender ) {
	found = whereFound;					/// A has-a relationship, not all palias are founded 
	species = "Loxioides bailleui";		/// Hardcode, is-a relationship, all palilas are the same species
	gender = newGender;					/// A has-a relationship, not all palilas are the same gender
	featherColor = newColor;			/// A has-a relationship, not all palilas have the same feather color
	isMigratory = false;				/// Hardcode, is-a relationship, palilas have either migrated or not 
}

/// Prints Palila first, if it was founded, whatever information Bird holds
void Palila::printInfo() {
	cout << "Palila" << endl;
	cout << "   Where Found = [" << found << "]" << endl;
	
	Bird::printInfo();
}

} // namespace animalfarm